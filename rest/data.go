/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package rest

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"librehomecareapi/contracts"
	"librehomecareapi/methods"
	"net/http"

	"github.com/jbrodriguez/mlog"
)

var (
	maxTimeMin = 10
	minTimeMin = 10
)

//AddData Handles store new Data request
func AddData(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf(errEmptyBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var data contracts.Data
	if err = json.Unmarshal(body, &data); err != nil {
		err = fmt.Errorf(errInvalidBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if data.PatientID == "" {
		err = fmt.Errorf(errEmptyFields)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(data.Data) == 0 {
		err = fmt.Errorf(errDataLenght)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	warn := methods.CheckTime(data.Date, minTimeMin, maxTimeMin)
	if warn != "" {
		mlog.Warning(warn)
	}

	if !methods.CheckUserByRoleAndID(patient, data.PatientID) {
		err = fmt.Errorf(errUserNotFound, patient, data.PatientID)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = methods.StoreNewData(data)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	mlog.Info(storedDataOK)
	w.WriteHeader(http.StatusCreated)
}

//GetAllData Handles get all Data request
func GetAllData(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf(errEmptyBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var data contracts.UserID
	if err = json.Unmarshal(body, &data); err != nil {
		err = fmt.Errorf(errInvalidBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if !methods.CheckUserByRoleAndID(patient, data.UserID) {
		err = fmt.Errorf(errUserNotFound, patient, data.UserID)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	alldata, err := methods.GetAllDataByPatientID(data.UserID)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(&alldata)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//GetDataByDates Handles get all Data by Dates request
func GetDataByDates(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf(errEmptyBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var data contracts.DataByDate
	if err = json.Unmarshal(body, &data); err != nil {
		err = fmt.Errorf(errInvalidBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if !methods.CheckUserByRoleAndID(patient, data.PatientID) {
		err = fmt.Errorf(errUserNotFound, patient, data.PatientID)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	alldata, err := methods.GetDataByPatientIDAndDates(data)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(&alldata)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//GetLastData Handles get last Data request
func GetLastData(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf(errEmptyBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var data contracts.User
	if err = json.Unmarshal(body, &data); err != nil {
		err = fmt.Errorf(errInvalidBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if !methods.CheckUserByRoleAndID(patient, data.UserID) {
		err = fmt.Errorf(errUserNotFound, patient, data.UserID)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	lastdata, err := methods.GetLastDataByPatientID(data.UserID)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(&lastdata)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

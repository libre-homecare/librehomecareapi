/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package rest

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"librehomecareapi/contracts"
	"librehomecareapi/methods"
	"net/http"

	"github.com/go-zoo/bone"
	"github.com/jbrodriguez/mlog"
)

//CreateUser Handles create a new User request
func CreateUser(w http.ResponseWriter, r *http.Request) {
	role := bone.GetValue(r, "role")
	if role != patient && role != doctor {
		err := fmt.Errorf(errInvalidRole)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf(errEmptyBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var data contracts.NewUser
	if err = json.Unmarshal(body, &data); err != nil {
		err = fmt.Errorf(errInvalidBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if (data.UserName == "" || data.Password == "" || data.FullName == "" || data.Email == "" || data.Phone == "") || (role == patient && data.DNI == "") {
		err = fmt.Errorf(errEmptyFields)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if !methods.CheckUserByRoleAndUserName(role, data.UserName) {
		err = fmt.Errorf(errAlreadyExists)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userID, err := methods.AuthRegisterNewUser(data.UserName, data.Password, role)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = methods.StoreNewUser(data, userID, role)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	mlog.Info(createdUserOK, role, data.UserName, userID)
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(userID))
}

//UpdateUser Handles create a new User request
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	role := bone.GetValue(r, "role")
	if role != patient && role != doctor {
		err := fmt.Errorf(errInvalidRole)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf(errEmptyBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var data contracts.UpdateUser
	if err = json.Unmarshal(body, &data); err != nil {
		err = fmt.Errorf(errInvalidBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if data.UserID == "" || (data.FullName == "" && data.DNI == "" && data.Email == "" && data.Phone == "") {
		err = fmt.Errorf(errEmptyFields)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if role == doctor && (data.FullName == "" && data.Email == "" && data.Phone == "") && data.DNI != "" {
		err = fmt.Errorf(errEmptyFields)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if !methods.CheckUserByRoleAndID(role, data.UserID) {
		err = fmt.Errorf(errUserNotFound, role, data.UserID)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = methods.UpdateUserByRoleAndID(role, data)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

//GetUserByID Handles get User by ID request
func GetUserByID(w http.ResponseWriter, r *http.Request) {
	role := bone.GetValue(r, "role")
	if role != patient && role != doctor {
		err := fmt.Errorf(errInvalidRole)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf(errEmptyBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var data contracts.UserID
	if err = json.Unmarshal(body, &data); err != nil {
		err = fmt.Errorf(errInvalidBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if data.UserID == "" {
		err = fmt.Errorf(errEmptyFields)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if !methods.CheckUserByRoleAndID(role, data.UserID) {
		err = fmt.Errorf(errUserNotFound, role, data.UserID)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := methods.GetUserByRoleAndID(role, data.UserID)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = json.NewEncoder(w).Encode(&user)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//DeleteUserByID Handles delete User by ID request
func DeleteUserByID(w http.ResponseWriter, r *http.Request) {
	role := bone.GetValue(r, "role")
	if role != patient && role != doctor {
		err := fmt.Errorf(errInvalidRole)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf(errEmptyBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var data contracts.UserID
	if err = json.Unmarshal(body, &data); err != nil {
		err = fmt.Errorf(errInvalidBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if data.UserID == "" {
		err = fmt.Errorf(errEmptyFields)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if !methods.CheckUserByRoleAndID(role, data.UserID) {
		err = fmt.Errorf(errUserNotFound, role, data.UserID)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = methods.AuthDeleteUser(role, data.UserID)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = methods.DeleteUser(role, data.UserID)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	mlog.Info(deletedUserOK, role, data.UserID)
	w.WriteHeader(http.StatusOK)
}

//ChangeUserPass Handles create a new User request
func ChangeUserPass(w http.ResponseWriter, r *http.Request) {
	role := bone.GetValue(r, "role")
	if role != patient && role != doctor {
		err := fmt.Errorf(errInvalidRole)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf(errEmptyBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var data contracts.ChangePass
	if err = json.Unmarshal(body, &data); err != nil {
		err = fmt.Errorf(errInvalidBody, err)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if data.UserID == "" || data.Password == "" || data.NewPassword == "" {
		err = fmt.Errorf(errEmptyFields)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if !methods.CheckUserByRoleAndID(role, data.UserID) {
		err = fmt.Errorf(errUserNotFound, role, data.UserID)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := methods.GetUserByRoleAndID(role, data.UserID)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = methods.AuthChangeUserPass(role, user.UserName, data.Password, data.NewPassword)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	mlog.Info(changePassOK, role, user.UserName)
	w.WriteHeader(http.StatusOK)
}

//GetAllUsers Handles get all Users request
func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	role := bone.GetValue(r, "role")
	if role != patient && role != doctor {
		err := fmt.Errorf(errInvalidRole)
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	allusers, err := methods.GetAllUsersByRole(role)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(&allusers)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

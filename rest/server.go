/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package rest

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-zoo/bone"
	"github.com/jbrodriguez/mlog"
)

const (
	ping = "/ping"
)

const (
	applicationJSON = "application/json; charset=utf-8"
)

func replyPing(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Write([]byte("pong"))
}

// LoadRestRoutes function
func httpServer() http.Handler {
	mux := bone.New()

	mux.Get(ping, http.HandlerFunc(replyPing))

	mux.Post("/user/:role", http.HandlerFunc(CreateUser))
	mux.Put("/user/:role", http.HandlerFunc(UpdateUser))
	mux.Get("/user/:role", http.HandlerFunc(GetUserByID))
	mux.Delete("/user/:role", http.HandlerFunc(DeleteUserByID))
	mux.Get("/users/:role", http.HandlerFunc(GetAllUsers))

	mux.Put("/changePass/:role", http.HandlerFunc(ChangeUserPass))

	mux.Post("/comment", http.HandlerFunc(AddComment))
	mux.Get("/comments", http.HandlerFunc(GetAllComments))

	mux.Post("/data", http.HandlerFunc(AddData))
	mux.Get("/data", http.HandlerFunc(GetAllData))
	mux.Get("/databydates", http.HandlerFunc(GetDataByDates))
	mux.Get("/lastdata", http.HandlerFunc(GetLastData))

	return mux
}

// StartHTTPServer starts HTTP server
func StartHTTPServer(errChan chan error, port int) {
	go func() {
		mlog.Info("Listening on port: " + strconv.Itoa(port))
		errChan <- http.ListenAndServe(":"+strconv.Itoa(port), httpServer())
	}()
}

func sendJSON(w http.ResponseWriter, v interface{}) {
	w.Header().Set("Content-Type", applicationJSON)
	err := json.NewEncoder(w).Encode(v)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

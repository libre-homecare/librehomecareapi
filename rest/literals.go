/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package rest

// Literals
var (
	patient          = "patient"
	doctor           = "doctor"
	createdUserOK    = "New %s %s registered with ID %s"
	deletedUserOK    = "Deleted %s with ID %s"
	changePassOK     = "%s %s password changed"
	createdCommentOK = "New comment registered"
	storedDataOK     = "New data stored"
)

// Errors returned
var (
	errInvalidRole   = "Invalid role"
	errEmptyBody     = "Empty body: %v"
	errInvalidBody   = "Invalid body %v"
	errEmptyFields   = "Empty fields"
	errUserNotFound  = "%s with ID %s not found"
	errAlreadyExists = "User Already Exists"
	errDataLenght    = "Data lenght is 0"
)

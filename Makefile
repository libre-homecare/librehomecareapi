#
#
#OximetryHomeCareAPI
#
#Copyright (c) 2020,
#Javier F. Montes Heras <yorozuya3@protonmail.com>
#Fco. Javier García Vázquez <xavie2a@gmail.com>
#
#OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
#Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#You should have received a copy of the GNU Affero General Public License along with this program.
#If not, see <http://www.gnu.org/licenses/>.
#
#*/

NAME=librehomecareapi
GO=GO111MODULE=on go
VERSION=$(shell cat ./VERSION)
GOFLAGS=-ldflags "-s -w -X main.version=$(VERSION)"


build:
	$(GO) build -o $(NAME)
	$(GO) mod vendor

arm:
	GOOS=linux GOARCH=arm $(GO) build $(GOFLAGS) -o $(NAME)

compress:
	upx-ucl --brute $(NAME)

clean:
	rm -f $(NAME)

test:
	GO111MODULE=on go test -cover
	GO111MODULE=on go vet
	gofmt -l .

run:
	./$(NAME)

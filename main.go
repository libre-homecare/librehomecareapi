/*

OximetryHomeCareAPI

Copyright (c) 2020, Javier Félix Montes Heras <yorozuya3@protonmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"fmt"
	"librehomecareapi/methods"
	"librehomecareapi/rest"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/jbrodriguez/mlog"
)

var version string

func main() {
	start := time.Now()

	mlog.DefaultFlags = log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile
	mlog.StartEx(mlog.LevelInfo, "OximetryHome.log", 256*1024, 4)

	mlog.Info("Started OximetryHome service v%s", version)

	errs := make(chan error, 2)
	listenForInterrupt(errs)
	rest.StartHTTPServer(errs, 3000)
	methods.InitDB("datos.sqlite")
	defer methods.CloseDB()

	mlog.Info("Service started in: " + time.Since(start).String())

	c := <-errs
	mlog.Warning("Terminated: %v", c)

	os.Exit(0)

}

func listenForInterrupt(errChan chan error) {
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-c)
	}()
}

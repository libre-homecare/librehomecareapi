module librehomecareapi

go 1.13

require (
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/go-zoo/bone v1.3.0
	github.com/gorilla/mux v1.7.4 // indirect
	github.com/jbrodriguez/mlog v0.0.0-20180805173533-cbd5ae8e9c53
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
)

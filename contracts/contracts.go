/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package contracts

// UserID structure
type UserID struct {
	UserID string
}

// User structure
type User struct {
	UserID   string
	UserName string
	FullName string
	DNI      string
	Email    string
	Phone    string
}

// NewUser structure
type NewUser struct {
	UserName string
	Password string
	FullName string
	DNI      string
	Email    string
	Phone    string
}

// UpdateUser structure
type UpdateUser struct {
	UserID   string
	FullName string
	DNI      string
	Email    string
	Phone    string
}

// ChangePass structure
type ChangePass struct {
	UserID      string
	Password    string
	NewPassword string
}

// Comment structure
type Comment struct {
	PatientID string
	DoctorID  string
	Comment   string
}

// CommentsResponse structure
type CommentsResponse struct {
	CommentID int
	PatientID string
	DoctorID  string
	Comment   string
}

// Data structure
type Data struct {
	PatientID string
	Date      int64
	Data      []DataValues
}

// DataValues structure
type DataValues struct {
	Bpm    int
	Spo2   int
	Temp   int
	Offset int
}

// DataByDate structure
type DataByDate struct {
	PatientID string
	DateINI   int64
	DateFIN   int64
}

// DataResponse structure
type DataResponse struct {
	Bpm  int
	Spo2 int
	Temp int
	Date int64
}

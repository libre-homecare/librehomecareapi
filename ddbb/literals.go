/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package ddbb

// Literals
var (
	prep     = "preparing"
	exec     = "executing"
	scan     = "scanning"
	patient  = "patient"
	doctor   = "doctor"
	comments = "comments"
	data     = "data"
)

// Errors
var (
	errOpeningDB     = "Error Opening Database: %v"
	errClosingDB     = "Error Closing Database: %v"
	errCreatingTable = "Error Creating %v Table: %v"

	errStoreUser             = "Error %v query in %v Table for creating user: %v"
	errUpdateUser            = "Error %v query in %v Table for updating user's %v: %v"
	errReaAllUsersByRole     = "Error %v query in %v Table for reading all users by role: %v"
	errReadUserByRoleAndID   = "Error %v query in %v Table for reading user by role and ID: %v"
	errDeleteUserByRoleAndID = "Error %v query in %v Table for deleting user by role and ID: %v"

	errStoreComment               = "Error %v query in Comments table for storing comment: %v"
	errReadAllCommentsByPatientID = "Error %v query in Comments table for reading all comments by patient ID: %v"

	errStoreData                   = "Error %v query in Data table for storing data: %v"
	errReadDataByPatientID         = "Error %v query in Data Table for reading data by patient ID: %v"
	errReadDataByPatientIDAndDates = "Error %v query in Data Table for reading data by patient ID and Dates: %v"
	errReadLastData                = "Error %v query in Data Table for reading last data: %v"
)

// Queries
var (
	sqlCreatePatientsTable = `CREATE TABLE IF NOT EXISTS patient( ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,UserID varchar(36) NOT NULL, UserName varchar(20) NOT NULL,
	FullName varchar(50) NOT NULL, DNI varchar(8) NOT NULL, Email varchar(50) NOT NULL, Phone varchar(20) NOT NULL );`
	sqlCreateDoctorsTable = `CREATE TABLE IF NOT EXISTS doctor( ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,	UserID varchar(36) NOT NULL, UserName varchar(20) NOT NULL,
	FullName varchar(50) NOT NULL, DNI varchar(8), Email varchar(50) NOT NULL, Phone varchar(20) NOT NULL );`
	sqlCreateDataTable = `CREATE TABLE IF NOT EXISTS data( ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, PatientID varchar(36) NOT NULL,
	Bpm int(11) DEFAULT NULL, Spo2 int(11) DEFAULT NULL, Temp int(11) DEFAULT NULL, DateTime datetime );`
	sqlCreateCommentsTable = `CREATE TABLE IF NOT EXISTS comments( ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, PatientID varchar(36) NOT NULL, DoctorID varchar(36) NOT NULL, Comment TEXT NOT NULL );`

	sqlStoreUser                  = "INSERT INTO %v (UserID, UserName, FullName, DNI, Email, Phone) values(?,?,?,?,?,?);"
	sqlUpdateUser                 = "UPDATE %v SET %v = ? WHERE UserID = ?;"
	sqlReaAllUsersByRole          = "SELECT UserID, UserName, FullName, DNI, Email, Phone FROM %v;"
	sqlReadUserByRoleAndID        = "SELECT UserID, UserName, FullName, DNI, Email, Phone FROM %v WHERE UserID = ?;"
	sqlCheckUserByRoleAndUserName = "SELECT UserID FROM %v WHERE UserName = ?;"
	sqlCheckUserByRoleAndID       = "SELECT UserID FROM %v WHERE UserID = ?;"
	sqlDeleteUserByRoleAndID      = "DELETE FROM %v WHERE UserID = ?;"

	sqlStoreComment               = "INSERT INTO comments (PatientID, DoctorID, Comment) values(?,?,?);"
	sqlReadAllCommentsByPatientID = "SELECT ID, PatientID, DoctorID, Comment FROM comments WHERE PatientID = ?;"

	sqlStoreData                   = "INSERT INTO data (PatientID, Bpm, Spo2, Temp, DateTime) values(?,?,?,?,?);"
	sqlReadDataByPatientID         = "SELECT Bpm, Spo2, Temp, DateTime FROM data WHERE PatientID = ?;"
	sqlReadDataByPatientIDAndDates = "SELECT Bpm, Spo2, Temp, DateTime FROM data WHERE PatientID = ? AND DateTime BETWEEN ? AND ?;"
	sqlReadLastData                = "SELECT Bpm, Spo2, Temp, DateTime FROM data WHERE PatientID = ? ORDER BY DateTime DESC LIMIT 1;"
)

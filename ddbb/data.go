/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package ddbb

import (
	"fmt"
	"librehomecareapi/contracts"
	"time"

	"github.com/jbrodriguez/mlog"
	_ "github.com/mattn/go-sqlite3" //go-sqlite3 import
)

// StoreData stores new Data in Data Table
func (sqlc *SQLClient) StoreData(patientID string, date int64, allvalues []contracts.DataValues) (err error) {
	stmt, err := sqlc.db.Prepare(sqlStoreData)
	if err != nil {
		err = fmt.Errorf(errStoreData, prep, err)
		return
	}
	defer stmt.Close()

	for _, values := range allvalues {
		newDate := date + int64(values.Offset)
		dt := time.Unix(newDate, 0)
		_, err = stmt.Exec(patientID, values.Bpm, values.Spo2, values.Temp, dt)
		if err != nil {
			mlog.Info(errStoreData, exec, err)
			return
		}
	}
	return
}

// ReadAllDataByPatientID reads all Data by Patient's ID from Data Table
func (sqlc *SQLClient) ReadAllDataByPatientID(patientID string) (alldata []contracts.DataResponse, err error) {
	rows, err := sqlc.db.Query(sqlReadDataByPatientID, patientID)
	if err != nil {
		err = fmt.Errorf(errReadDataByPatientID, prep, err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		data := contracts.DataResponse{}
		var dt time.Time
		err = rows.Scan(&data.Bpm, &data.Spo2, &data.Temp, &dt)
		if err != nil {
			err = fmt.Errorf(errReadDataByPatientID, exec, err)
			return
		}
		data.Date = dt.Unix()
		alldata = append(alldata, data)
	}
	return
}

// ReadDataByPatientIDAndDates reads all Data by Patient's ID & Dates from Data Table
func (sqlc *SQLClient) ReadDataByPatientIDAndDates(dataByDate contracts.DataByDate) (alldata []contracts.DataResponse, err error) {
	rows, err := sqlc.db.Query(sqlReadDataByPatientIDAndDates, dataByDate.PatientID, time.Unix(dataByDate.DateINI, 0), time.Unix(dataByDate.DateFIN, 0))
	if err != nil {
		err = fmt.Errorf(errReadDataByPatientIDAndDates, prep, err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		data := contracts.DataResponse{}
		var dt time.Time
		err = rows.Scan(&data.Bpm, &data.Spo2, &data.Temp, &dt)
		if err != nil {
			err = fmt.Errorf(errReadDataByPatientIDAndDates, scan, err)
			return
		}
		data.Date = dt.Unix()
		alldata = append(alldata, data)
	}
	return
}

// ReadLastDataByPatientID reads last Data by Patient's ID from Data Table
func (sqlc *SQLClient) ReadLastDataByPatientID(patientID string) (lastData contracts.DataResponse, err error) {
	var dt time.Time
	err = sqlc.db.QueryRow(sqlReadLastData, patientID).Scan(&lastData.Bpm, &lastData.Spo2, &lastData.Temp, &dt)
	if err != nil {
		err = fmt.Errorf(errReadLastData, exec, err)
		return
	}
	lastData.Date = dt.Unix()
	return
}

/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package ddbb

import (
	"fmt"
	"librehomecareapi/contracts"

	_ "github.com/mattn/go-sqlite3" //go-sqlite3 import
)

// StoreComment stores new Comment in Comments Table
func (sqlc *SQLClient) StoreComment(comment contracts.Comment) (err error) {
	stmt, err := sqlc.db.Prepare(sqlStoreComment)
	if err != nil {
		err = fmt.Errorf(errStoreComment, prep, err)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(comment.PatientID, comment.DoctorID, comment.Comment)
	if err != nil {
		err = fmt.Errorf(errStoreComment, exec, err)
		return
	}
	return
}

// ReadAllCommentsByPatientID reads all Comments by Patient's ID from Comments Table
func (sqlc *SQLClient) ReadAllCommentsByPatientID(patientID string) (comments []contracts.CommentsResponse, err error) {
	rows, err := sqlc.db.Query(sqlReadAllCommentsByPatientID, patientID)
	if err != nil {
		err = fmt.Errorf(errReadAllCommentsByPatientID, prep, err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		comment := contracts.CommentsResponse{}
		err = rows.Scan(&comment.CommentID, &comment.PatientID, &comment.DoctorID, &comment.Comment)
		if err != nil {
			err = fmt.Errorf(errReadAllCommentsByPatientID, exec, err)
			return
		}
		comments = append(comments, comment)
	}
	return
}

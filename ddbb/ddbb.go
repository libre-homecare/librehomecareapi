/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package ddbb

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3" //go-sqlite3 import
)

// SQLClient is the SQL client
type SQLClient struct {
	db *sql.DB // Bolt database
}

// OpenDatabase opens Database
func OpenDatabase(filepath string) (*SQLClient, error) {
	sqldb, err := sql.Open("sqlite3", filepath)
	if err != nil {
		err = fmt.Errorf(errOpeningDB, err)
		return nil, err
	}

	sqlClient := &SQLClient{db: sqldb}
	return sqlClient, nil
}

// CloseDatabase closes Database
func (sqlc *SQLClient) CloseDatabase() (err error) {
	err = sqlc.db.Close()
	if err != nil {
		err = fmt.Errorf(errOpeningDB, err)
		return
	}
	return
}

// CreateTables creates Database if needed
func (sqlc *SQLClient) CreateTables() (err error) {

	_, err = sqlc.db.Exec(sqlCreatePatientsTable)
	if err != nil {
		err = fmt.Errorf(errCreatingTable, patient, err)
		return
	}

	_, err = sqlc.db.Exec(sqlCreateDoctorsTable)
	if err != nil {
		err = fmt.Errorf(errCreatingTable, doctor, err)
		return
	}

	_, err = sqlc.db.Exec(sqlCreateCommentsTable)
	if err != nil {
		err = fmt.Errorf(errCreatingTable, comments, err)
		return
	}

	_, err = sqlc.db.Exec(sqlCreateDataTable)
	if err != nil {
		err = fmt.Errorf(errCreatingTable, data, err)
		return
	}

	return err
}

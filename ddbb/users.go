/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package ddbb

import (
	"fmt"
	"librehomecareapi/contracts"

	_ "github.com/mattn/go-sqlite3" //go-sqlite3 import
)

// StoreUser stores new User by User's Role in its Table
func (sqlc *SQLClient) StoreUser(newUser contracts.NewUser, id string, role string) (err error) {
	stmt, err := sqlc.db.Prepare(fmt.Sprintf(sqlStoreUser, role))
	if err != nil {
		err = fmt.Errorf(errStoreUser, prep, role, err)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(id, newUser.UserName, newUser.FullName, newUser.DNI, newUser.Email, newUser.Phone)
	if err != nil {
		err = fmt.Errorf(errStoreUser, exec, role, err)
		return
	}

	return
}

// UpdateUserElement stores new User by User's Role in its Table
func (sqlc *SQLClient) UpdateUserElement(role, id, element, value string) (err error) {
	stmt, err := sqlc.db.Prepare(fmt.Sprintf(sqlUpdateUser, role, element))
	if err != nil {
		err = fmt.Errorf(errUpdateUser, prep, role, element, err)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(value, id)
	if err != nil {
		err = fmt.Errorf(errStoreUser, exec, role, element, err)
		return
	}
	return
}

// ReadAllUsersByRole reads all Users by User's Role from its Table
func (sqlc *SQLClient) ReadAllUsersByRole(role string) (result []contracts.User, err error) {
	rows, err := sqlc.db.Query(fmt.Sprintf(sqlReaAllUsersByRole, role))
	if err != nil {
		err = fmt.Errorf(errReaAllUsersByRole, prep, role, err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		patient := contracts.User{}
		err = rows.Scan(&patient.UserID, &patient.UserName, &patient.FullName, &patient.DNI, &patient.Email, &patient.Phone)
		if err != nil {
			err = fmt.Errorf(errReaAllUsersByRole, exec, role, err)
			return
		}
		result = append(result, patient)
	}
	return
}

// ReadUserByRoleAndID reads User by User's Role and ID from its Table
func (sqlc *SQLClient) ReadUserByRoleAndID(role, id string) (user contracts.User, err error) {
	err = sqlc.db.QueryRow(fmt.Sprintf(sqlReadUserByRoleAndID, role), id).Scan(&user.UserID, &user.UserName, &user.FullName, &user.DNI, &user.Email, &user.Phone)
	if err != nil {
		err = fmt.Errorf(errReadUserByRoleAndID, exec, role, err)
		return
	}
	return
}

// CheckUserByRoleAndUserName checks User's existence by User's Role and UserName from its Table
func (sqlc *SQLClient) CheckUserByRoleAndUserName(role, userName string) (err error) {
	err = sqlc.db.QueryRow(fmt.Sprintf(sqlCheckUserByRoleAndUserName, role), userName).Scan(&userName)
	if err != nil {
		return
	}
	return
}

// CheckUserByRoleAndID checks User's existence by User's Role and ID from its Table
func (sqlc *SQLClient) CheckUserByRoleAndID(role, id string) (err error) {
	err = sqlc.db.QueryRow(fmt.Sprintf(sqlCheckUserByRoleAndID, role), id).Scan(&id)
	if err != nil {
		return
	}
	return
}

// DeleteUserByRoleAndID deletes User by User's Role and ID from its Table
func (sqlc *SQLClient) DeleteUserByRoleAndID(role, id string) (err error) {
	stmt, err := sqlc.db.Prepare(fmt.Sprintf(sqlDeleteUserByRoleAndID, role))
	if err != nil {
		err = fmt.Errorf(errDeleteUserByRoleAndID, prep, role, err)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(id)
	if err != nil {
		err = fmt.Errorf(errDeleteUserByRoleAndID, exec, role, err)
		return
	}

	return
}

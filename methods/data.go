/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package methods

import (
	"errors"
	"fmt"
	"librehomecareapi/contracts"
	"math"
	"time"

	"github.com/jbrodriguez/mlog"
)

// Maxs and Mins
var (
	BpmMIN  = 0
	BpmMAX  = 10
	Spo2MIN = 0
	Spo2MAX = 10
	TempMIN = 0
	TempMAX = 10
)

// StoreNewData calls database's mew Data storing method
func StoreNewData(data contracts.Data) (err error) {

	var newDataValues []contracts.DataValues
	for i, dataValue := range data.Data {
		if dataValue.Bpm > BpmMAX || dataValue.Bpm < BpmMIN {
			mlog.Warning(warnBpmVal, dataValue.Bpm, i)
			dataValue.Bpm = 0
		}
		if dataValue.Spo2 > Spo2MAX || dataValue.Spo2 < Spo2MIN {
			mlog.Warning(warnSpo2Val, dataValue.Spo2, i)
			dataValue.Spo2 = 0
		}
		if dataValue.Temp > TempMAX || dataValue.Temp < TempMIN {
			mlog.Warning(warnTempVal, dataValue.Temp, i)
			dataValue.Temp = 0
		}
		if dataValue.Bpm == 0 && dataValue.Spo2 == 0 && dataValue.Temp == 0 {
			rowErr := fmt.Errorf(errRowVals, i)
			mlog.Error(rowErr)
			continue
		}
		newDataValues = append(newDataValues, dataValue)
	}

	if len(newDataValues) == 0 {
		err = errors.New(errDataLenght)
		return
	}

	err = dbClient.StoreData(data.PatientID, data.Date, newDataValues)
	return
}

// GetAllDataByPatientID calls database's all Data reading by Patient's ID method
func GetAllDataByPatientID(id string) (alldata []contracts.DataResponse, err error) {
	alldata, err = dbClient.ReadAllDataByPatientID(id)
	return
}

// GetDataByPatientIDAndDates calls database's all Data reading by Patient's ID & Dates method
func GetDataByPatientIDAndDates(dataByDate contracts.DataByDate) (alldata []contracts.DataResponse, err error) {
	alldata, err = dbClient.ReadDataByPatientIDAndDates(dataByDate)
	return
}

// GetLastDataByPatientID calls database's last Data reading by Patient's ID method
func GetLastDataByPatientID(id string) (lastData contracts.DataResponse, err error) {
	lastData, err = dbClient.ReadLastDataByPatientID(id)
	return
}

// CheckTime gets all users
func CheckTime(dataTimeReceived int64, minTimeMin int, maxTimeMin int) (warn string) {
	tNow := time.Now()
	tMax := tNow.Add(time.Minute * time.Duration(maxTimeMin))
	tMin := tNow.Add(time.Minute * -time.Duration(minTimeMin))
	tReceived := time.Unix(dataTimeReceived, 0)

	if tReceived.Before(tMin) {
		diff := tNow.Sub(tReceived)
		formatedDate := timeDiffToString(diff)
		rReceivedFormatted := fmt.Sprintf("%02d/%02d/%d %02d:%02d:%02d", tReceived.Day(), tReceived.Month(), tReceived.Year(), tReceived.Hour(), tReceived.Minute(), tReceived.Second())
		warn = fmt.Sprintf(warnBeforeDate, rReceivedFormatted, formatedDate)
	}

	if tReceived.After(tMax) {
		diff := tReceived.Sub(tNow)
		formatedDate := timeDiffToString(diff)
		rReceivedFormatted := fmt.Sprintf("%02d/%02d/%d %02d:%02d:%02d", tReceived.Day(), tReceived.Month(), tReceived.Year(), tReceived.Hour(), tReceived.Minute(), tReceived.Second())
		warn = fmt.Sprintf(warnAfterDate, rReceivedFormatted, formatedDate)
	}
	return
}

// TimeDiffToString gets all users
func timeDiffToString(diff time.Duration) (formatedDate string) {
	diff = diff.Round(time.Second)
	seconds := diff.Seconds()
	days := math.Floor(seconds / 86400)
	hours := math.Floor(seconds / 3600)
	minutes := math.Floor(seconds / 60)

	if diff.Hours() > 24 {
		hours = hours - days*24
		formatedDate = fmt.Sprintf("%v days, ", days)
	}

	if diff.Hours() > 1 {
		minutes = minutes - hours*60 - days*1440
		formatedDate = formatedDate + fmt.Sprintf("%v hours, ", hours)
	}

	if diff.Minutes() > 1 {
		seconds = seconds - minutes*60 - hours*3600 - days*86400
		formatedDate = formatedDate + fmt.Sprintf("%v minutes and ", minutes)
	}

	formatedDate = formatedDate + fmt.Sprintf("%v seconds", seconds)
	return
}

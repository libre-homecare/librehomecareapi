/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package methods

import (
	"librehomecareapi/ddbb"

	"github.com/jbrodriguez/mlog"
)

var dbClient *ddbb.SQLClient

// InitDB calls database's Opening method & Tables creating if needed
func InitDB(filepath string) {
	var err error
	dbClient, err = ddbb.OpenDatabase(filepath)
	if err != nil {
		mlog.Error(err)
	}
	err = dbClient.CreateTables()
	if err != nil {
		mlog.Error(err)
	}

}

// CloseDB calls database's Closing method
func CloseDB() {
	var err error
	err = dbClient.CloseDatabase()
	if err != nil {
		mlog.Error(err)
	}
}

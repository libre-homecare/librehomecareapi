/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package methods

import "librehomecareapi/contracts"

// StoreNewComment calls database's mew Comment storing method
func StoreNewComment(comment contracts.Comment) (err error) {
	err = dbClient.StoreComment(comment)
	return err
}

// ReadAllCommentsByPatientID calls database's all Comment reading by Patient's ID method
func ReadAllCommentsByPatientID(id string) (allcomments []contracts.CommentsResponse, err error) {
	allcomments, err = dbClient.ReadAllCommentsByPatientID(id)
	return
}

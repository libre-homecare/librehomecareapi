/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package methods

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
)

// AuthRegisterNewUser tries to register new User in Auth Service
func AuthRegisterNewUser(username, password, role string) (UserID string, err error) {
	var jsonStr = []byte(fmt.Sprintf(newUserjson, username, password, role))
	req, err := http.NewRequest("POST", url+"users", bytes.NewBuffer(jsonStr))
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusCreated {
		err = fmt.Errorf(errCreatingAth, role, string(body))
		return
	}
	UserID = string(body)
	return
}

// AuthDeleteUser tries to delete User in Auth Service
func AuthDeleteUser(role, id string) (err error) {
	req, err := http.NewRequest("DELETE", url+"users/"+id, nil)
	if err != nil {
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf(errDeletingAth, role, string(body))
		return
	}
	return
}

// AuthChangeUserPass tries to change User password
func AuthChangeUserPass(role, username, password, newpassword string) (err error) {
	var jsonStr = []byte(fmt.Sprintf(changePassjson, username, password, newpassword, role))
	req, err := http.NewRequest("PUT", url+"changePass", bytes.NewBuffer(jsonStr))
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf(errChangingPassAth, role, string(body))
		return
	}
	return
}

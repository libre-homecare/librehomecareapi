/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package methods

import (
	"fmt"
	"librehomecareapi/contracts"
)

// GetAllUsersByRole calls database's all Users reading by Users Role method
func GetAllUsersByRole(role string) (allusers []contracts.User, err error) {
	allusers, err = dbClient.ReadAllUsersByRole(role)
	return
}

// GetUserByRoleAndID calls database's User reading by User's Role & ID method
func GetUserByRoleAndID(role, id string) (user contracts.User, err error) {
	user, err = dbClient.ReadUserByRoleAndID(role, id)
	return
}

// CheckUserByRoleAndID calls database's User cheking by User's Role & ID method
func CheckUserByRoleAndID(role, id string) bool {
	err := dbClient.CheckUserByRoleAndID(role, id)
	if err != nil {
		return false
	}
	return true
}

// CheckUserByRoleAndUserName calls database's User cheking by User's Role & UserName method
func CheckUserByRoleAndUserName(role, userName string) bool {
	err := dbClient.CheckUserByRoleAndUserName(role, userName)
	if err != nil {
		return true
	}
	return false
}

// StoreNewUser calls database's mew User storing method
func StoreNewUser(newUser contracts.NewUser, id string, role string) (err error) {
	err = dbClient.StoreUser(newUser, id, role)
	return err
}

// UpdateUserByRoleAndID calls database's mew User storing method
func UpdateUserByRoleAndID(role string, data contracts.UpdateUser) (err error) {
	var updateError string
	if data.FullName != "" {
		err := dbClient.UpdateUserElement(role, data.UserID, fullName, data.FullName)
		if err != nil {
			updateError = fmt.Sprintf(errUPD, fullName, err)
		}
	}

	if role == patient && data.DNI != "" {
		err := dbClient.UpdateUserElement(role, data.UserID, dni, data.DNI)
		if err != nil {
			updateError = updateError + fmt.Sprintf(errUPD, dni, err)
		}
	}

	if data.Phone != "" {
		err := dbClient.UpdateUserElement(role, data.UserID, phone, data.Phone)
		if err != nil {
			updateError = updateError + fmt.Sprintf(errUPD, phone, err)
		}
	}

	if data.Email != "" {
		err := dbClient.UpdateUserElement(role, data.UserID, email, data.Email)
		if err != nil {
			updateError = updateError + fmt.Sprintf(errUPD, email, err)
		}
	}

	if updateError != "" {
		err = fmt.Errorf(errUpdatingUser, role, updateError)
	}
	return
}

// DeleteUser calls database's User deleting method
func DeleteUser(role, id string) (err error) {
	err = dbClient.DeleteUserByRoleAndID(role, id)
	return err
}

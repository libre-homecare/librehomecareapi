/*

OximetryHomeCareAPI

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

OximetrysHomeCareAPI is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package methods

// Literals
var (
	url            = "http://localhost:8080/"
	newUserjson    = `{"username":"%v","password":"%v","role":"%v"}`
	changePassjson = `{"username":"%v","password":"%v","newpassword":"%v","role":"%v"}`
	patient        = "patient"
	doctor         = "doctor"
	fullName       = "FullName"
	dni            = "DNI"
	phone          = "Phone"
	email          = "Email"
)

// Errors & Warnings
var (
	errCreatingAth     = "Error registering new %s in Auth Service, response: %s"
	errDeletingAth     = "Error deleting %s in Auth Service, response: %s"
	errChangingPassAth = "Error changing %s's password in Auth Service, response: %s"

	errUpdatingUser = "Error updating %s values: %s"
	errUPD          = "%s (%v); "

	errRowVals    = "Not a single valid value in this row, avoiding row nº%d"
	errDataLenght = "Not a single valid row in received Data"
	warnBpmVal    = "Invalid Bpm value %d at row nº%d"
	warnSpo2Val   = "Invalid Spo2 value %d at row nº%d"
	warnTempVal   = "Invalid Temp value %d at row nº%d"

	warnAfterDate  = "Data's time received (%s UTC) is future: %s after actual time"
	warnBeforeDate = "Data's time received (%s UTC) is past: %s before actual time"
)
